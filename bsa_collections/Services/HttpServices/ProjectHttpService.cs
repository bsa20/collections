﻿using bsa_collections.Models;
using bsa_collections.Services.HttpServices.Abstract;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace bsa_collections.Services.HttpServices
{
    public class ProjectHttpService : HttpService
    {
        public async Task<IEnumerable<Project>> GetAllProjectsAsync()
        {
            var jsonString = await this.GetRequest("/api/Projects");
            var result = JsonConvert.DeserializeObject<IEnumerable<Project>>(jsonString);

            return result;
        }

        public async Task<Project> GetProjectByIdAsync(int id)
        {
            var jsonString = await this.GetRequest("/api/Projects/" + id);
            var result = JsonConvert.DeserializeObject<Project>(jsonString);

            return result;
        }
    }
}
