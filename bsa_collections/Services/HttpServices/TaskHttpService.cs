﻿using bsa_collections.Services.HttpServices.Abstract;
using Newtonsoft.Json;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace bsa_collections.Services.HttpServices
{
    public class TaskHttpService : HttpService
    {
        public async Task<IEnumerable<Models.Task>> GetAllTasksAsync()
        {
            var jsonString = await this.GetRequest("/api/Tasks");
            var result = JsonConvert.DeserializeObject<IEnumerable<Models.Task>>(jsonString);

            return result;
        }

        public async Task<Models.Task> GetTaskByIdAsync(int id)
        {
            var jsonString = await this.GetRequest("/api/Tasks" + id);
            var result = JsonConvert.DeserializeObject<Models.Task>(jsonString);

            return result;
        }
    }
}
