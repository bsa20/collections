﻿using bsa_collections.Models;
using bsa_collections.Services.HttpServices.Abstract;
using Newtonsoft.Json;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace bsa_collections.Services.HttpServices
{
    public class TeamHttpService : HttpService
    {
        public async Task<IEnumerable<Team>> GetAllTeamsAsync()
        {
            var jsonString = await this.GetRequest("/api/Teams");
            var result = JsonConvert.DeserializeObject<IEnumerable<Team>>(jsonString);

            return result;
        }

        public async Task<Team> GetTeamByIdAsync(int id)
        {
            var jsonString = await this.GetRequest("/api/Teams/" + id);
            var result = JsonConvert.DeserializeObject<Team>(jsonString);

            return result;
        }
    }
}
