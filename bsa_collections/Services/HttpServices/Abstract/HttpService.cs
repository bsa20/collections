﻿using System.Net.Http;
using System.Net.Http.Headers;
using System.Threading.Tasks;

namespace bsa_collections.Services.HttpServices.Abstract
{
    public abstract class HttpService
    {
        protected readonly HttpClient httpClient;
        protected const string URL = "https://bsa20.azurewebsites.net";

        public HttpService()
        {
            this.httpClient = new HttpClient();
            this.httpClient.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
        }

        protected async Task<string> GetRequest(string sufix)
        {
            var httpResponse = await this.httpClient.GetAsync(URL + sufix);
            httpResponse.EnsureSuccessStatusCode();
            var jsonString = await httpResponse.Content.ReadAsStringAsync();

            return jsonString;
        }
    }
}
