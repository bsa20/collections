﻿using bsa_collections.Models;
using bsa_collections.Services.HttpServices.Abstract;
using Newtonsoft.Json;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace bsa_collections.Services.HttpServices
{
    public class UserHttpService : HttpService
    {
        public async Task<IEnumerable<User>> GetAllUsersAsync()
        {
            var jsonString = await this.GetRequest("/api/Users");
            var result = JsonConvert.DeserializeObject<IEnumerable<User>>(jsonString);

            return result;
        }

        public async Task<User> GetUserByIdAsync(int id)
        {
            var jsonString = await this.GetRequest("/api/Users/" + id);
            var result = JsonConvert.DeserializeObject<User>(jsonString);

            return result;
        }
    }
}
