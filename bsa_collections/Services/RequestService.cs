﻿using bsa_collections.DTO;
using bsa_collections.Enums;
using bsa_collections.Models;
using bsa_collections.Services.HttpServices;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace bsa_collections.Services
{
    public class RequestService
    {
        private UserHttpService _userHttpService;
        private ProjectHttpService _projectHttpService;
        private TaskHttpService _taskHttpService;
        private TeamHttpService _teamHttpService;
        private IEnumerable<Project> projects;
        private IEnumerable<User> users;
        private IEnumerable<Team> teams;
        private IEnumerable<Models.Task> tasks;

        public RequestService(UserHttpService userHttpService, ProjectHttpService projectHttpService, TaskHttpService taskHttpService, TeamHttpService teamHttpService)
        {
            _userHttpService = userHttpService;
            _projectHttpService = projectHttpService;
            _taskHttpService = taskHttpService;
            _teamHttpService = teamHttpService;

            GetDataFromAPI();
        }

        private async void GetDataFromAPI()
        {
            this.projects = await _projectHttpService.GetAllProjectsAsync();
            this.users = await _userHttpService.GetAllUsersAsync();
            this.teams = await _teamHttpService.GetAllTeamsAsync();
            this.tasks = await _taskHttpService.GetAllTasksAsync();

            this.users = (from user in users
                          join team in teams
                          on user.TeamId equals team.Id
                          select loadUser(user, team)).Union(
                          from user in users
                          where user.TeamId == null
                          select user
                          ).ToList();

            this.tasks = (from task in tasks
                          join project in projects
                          on task.ProjectId equals project.Id
                          join user in users
                          on task.PerformerId equals user.Id
                          select loadTask(task, project, user)).ToList();

            this.projects = (from project in projects
                            join author in users
                            on project.AuthorId equals author.Id
                            join task in tasks
                            on project.Id equals task.ProjectId
                            into tasksOfProject
                            join team in teams
                            on project.TeamId equals team.Id
                            select loadProject(project, tasksOfProject.ToList(), author, team)).ToList();
        }

        // 1
        public IDictionary<int, int> GetCountOfTasksOfUserInProject(int userId)
        {
            return projects.ToDictionary(p => p.Id, t => t.Tasks.Count(ts => ts.PerformerId == userId));
        }

        // 2
        public IEnumerable<Models.Task> GetTasksOfUser(int userId)
        {
            return tasks.Where(t => t.PerformerId == userId && t.Name.Length < 45).ToList();
        }

        // 3
        public IEnumerable<TaskDTO> GetTasksOfUserFinishedThisYear(int userId)
        {
            return tasks.Where(t => t.PerformerId == userId && t.State == TaskState.Finished && t.FinishedAt.Year == 2020)
                        .Select(t => new TaskDTO() { Id = t.Id, Name = t.Name }).ToList();
        }

        // 4
        public IEnumerable<TeamDTO> GetTeamsWithUsersOlderThanTenYears()
        {
            return teams.GroupJoin(users.Where(u => CalculateAge(u.Birthday) > 10), t => t.Id, u => u.TeamId, (t, u) => new TeamDTO
            {
                Id = t.Id,
                Name = t.Name,
                Users = u.OrderByDescending(u => u.RegisteredAt).ToList()
            }).Where(t => t.Users.Any()).ToList();
        }

        // 5
        public IEnumerable<UserWithTasksDTO> GetSortedUsersWithSortedTasks()
        {
            return users.OrderBy(u => u.FirstName)
                .GroupJoin(tasks, u => u.Id, t => t.PerformerId, (u, t) => new UserWithTasksDTO
                {
                    User = u,
                    Tasks = t.OrderByDescending(t => t.Name.Length).ToList()
                }).ToList();
        }

        // 6
        public UserMainInfoDTO GetMainInformationAboutUser(int userId)
        {
            return projects.Where(p => p.AuthorId == userId)
                        .OrderByDescending(p => p.CreatedAt).Take(1)
                        .Select(p => new UserMainInfoDTO
                        {
                            User = users.FirstOrDefault(u => u.Id == userId),
                            LastProject = p,
                            NumberOfTasksOfLastProject = p.Tasks.Count(),
                            NumberOfNotFinishedTasks = tasks.Count(t => t.PerformerId == userId && t.State != TaskState.Finished),
                            LongestTask = tasks.Where(t => t.PerformerId == userId)
                                               .Aggregate((max, t) => (t.FinishedAt - t.CreatedAt > max.FinishedAt - max.CreatedAt ? t : max))
                        }).First();
        }

        // 7
        public IEnumerable<ProjectMainInfoDTO> GetMainInfoAboutProjects()
        {
            return projects.Select(p => new ProjectMainInfoDTO
            {
                Project = p,
                LongestTaskByDescription = p.Tasks.Count() == 0 ? null : p.Tasks.Aggregate((max, t) => (t.Description.Length > max.Description.Length ? t : max)),
                ShortetTaskByName = p.Tasks.Count() == 0 ? null : p.Tasks.Aggregate((min, t) => (t.Name.Length < min.Name.Length ? t : min)),
                NumberOfUsersInTeam = (p.Description.Length > 20 || p.Tasks.Count() < 3) ? users.Count(u => u.TeamId == p.TeamId) : 0
            }).ToList();
        }

        private Project loadProject(Project project, IEnumerable<Models.Task> tasks, User author, Team team)
        {
            project.Tasks = tasks;
            project.Author = author;
            project.Team = team;
            return project;
        }

        private Models.Task loadTask(Models.Task task, Project project, User performer)
        {
            task.Project = project;
            task.Performer = performer;
            return task;
        }

        private User loadUser(User user, Team team)
        {
            user.Team = team;
            return user;
        }

        private int CalculateAge(DateTime dateOfBirth)
        {
            int age = 0;
            age = DateTime.Now.Year - dateOfBirth.Year;
            if (DateTime.Now.DayOfYear < dateOfBirth.DayOfYear)
                age = age - 1;

            return age;
        }
    }
}
