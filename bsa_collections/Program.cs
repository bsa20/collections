﻿using bsa_collections.DTO;
using bsa_collections.Models;
using bsa_collections.Services;
using bsa_collections.Services.HttpServices;
using Newtonsoft.Json;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;

namespace bsa_collections
{
    class Program
    {
        static void Main(string[] args)
        {
            var projectHttpService = new ProjectHttpService();
            var taskHttpService = new TaskHttpService();
            var teamHttpService = new TeamHttpService();
            var userHttpService = new UserHttpService();

            var requestService = new RequestService(userHttpService, projectHttpService, taskHttpService, teamHttpService);

            runMenu(requestService);
        }

        private static void runMenu(RequestService requestService)
        {
            while(true) {
                Console.WriteLine("1 : Get number of tasks of user in project (task 1)\n" +
                    "2 : Get tasks of user with name length <45 (task 2)\n" +
                    "3 : Get tasks of user finished in 2020 (task 3)\n" +
                    "4 : Get teams where all users older than 10 (task 4)\n" +
                    "5 : Get users order alphabeticaly with their tasks order by name length (task 5)\n" +
                    "6 : Get main info about user (task 6)\n" +
                    "7 : Get main info abaout all projects (task 7)\n"+
                    "0 : Exit");
                int x;
                try { x = int.Parse(Console.ReadLine().Trim()); }
                catch
                {
                    Console.Clear();
                    Console.ForegroundColor = ConsoleColor.Red;
                    Console.WriteLine("Enter number");
                    Console.ResetColor();
                    continue;
                }

                switch (x)
                {
                    case 1:
                        {
                            Console.WriteLine("Enter id of user");
                            int id;
                            try { id = int.Parse(Console.ReadLine().Trim()); }
                            catch
                            {
                                Console.Clear();
                                Console.ForegroundColor = ConsoleColor.Red;
                                Console.WriteLine("Enter number");
                                Console.ResetColor();
                                continue;
                            }
                            var result = requestService.GetCountOfTasksOfUserInProject(id);
                            foreach (var item in result)
                            {
                                Console.WriteLine($"Project id: {item.Key}, count of tasks: {item.Value}");
                            }
                            break;
                        }
                    case 2:
                        {
                            Console.WriteLine("Enter id of user");
                            int id;
                            try { id = int.Parse(Console.ReadLine().Trim()); }
                            catch
                            {
                                Console.Clear();
                                Console.ForegroundColor = ConsoleColor.Red;
                                Console.WriteLine("Enter number");
                                Console.ResetColor();
                                continue;
                            }
                            var result = requestService.GetTasksOfUser(id);
                            foreach (var item in result)
                            {
                                Console.WriteLine($"Task id: {item.Id}, Project id: {item.ProjectId}, Name: {item.Name}");
                            }
                            if(result.Count() == 0) Console.WriteLine("None");
                            break;
                        }
                    case 3:
                        {
                            Console.WriteLine("Enter id of user");
                            int id;
                            try { id = int.Parse(Console.ReadLine().Trim()); }
                            catch
                            {
                                Console.Clear();
                                Console.ForegroundColor = ConsoleColor.Red;
                                Console.WriteLine("Enter number");
                                Console.ResetColor();
                                continue;
                            }
                            var result = requestService.GetTasksOfUserFinishedThisYear(id);
                            foreach (var item in result)
                            {
                                Console.WriteLine($"Task id: {item.Id}, Name: {item.Name}");
                            }
                            if (result.Count() == 0) Console.WriteLine("None");
                            break;
                        }
                    case 4:
                        {
                            var result = requestService.GetTeamsWithUsersOlderThanTenYears();
                            foreach (var team in result)
                            {
                                Console.WriteLine($"Team id: {team.Id}, Team: {team.Name}");
                                foreach (var user in team.Users)
                                {
                                    Console.WriteLine($"\t{user.FirstName} {user.LastName}, registered at: {user.RegisteredAt}");
                                }
                            }
                            break;
                        }
                    case 5:
                        {
                            var result = requestService.GetSortedUsersWithSortedTasks();
                            foreach (var item in result)
                            {
                                Console.WriteLine($"{item.User.FirstName} {item.User.LastName}");
                                foreach (var task in item.Tasks)
                                {
                                    Console.WriteLine($"\tTask: id({task.Id}) {task.Name}");
                                }
                            }
                            break;
                        }
                    case 6:
                        {
                            Console.WriteLine("Enter id of user");
                            int id;
                            try { id = int.Parse(Console.ReadLine().Trim()); }
                            catch
                            {
                                Console.Clear();
                                Console.ForegroundColor = ConsoleColor.Red;
                                Console.WriteLine("Enter number");
                                Console.ResetColor();
                                continue;
                            }
                            UserMainInfoDTO result;
                            try
                            {
                                result = requestService.GetMainInformationAboutUser(id);
                            } catch (Exception e)
                            {
                                Console.WriteLine(e.Message);
                                break;
                            }
                            Console.WriteLine($"{result.User.FirstName} {result.User.LastName}\n" +
                                                $"Last project: id ({result.LastProject.Id}) name ({result.LastProject.Name})\n" +
                                                $"Number of taks on last project: {result.NumberOfTasksOfLastProject}\n" +
                                                $"Number of not finished tasks: {result.NumberOfNotFinishedTasks}\n" +
                                                $"Longest task: id ({result.LongestTask.Id}) name ({result.LongestTask.Name})\n");
                            break;
                        }
                    case 7:
                        {
                            var result = requestService.GetMainInfoAboutProjects();
                            foreach (var item in result)
                            {
                                Console.WriteLine($"Project id: {item.Project.Id}, Name: {item.Project.Name}");
                                if (item.LongestTaskByDescription != null) {
                                    Console.WriteLine($"Longest task of project by description: id({item.LongestTaskByDescription.Id})\n" +
                                                        $"Shortest task of project by name: id({item.ShortetTaskByName.Id})");
                                }
                                Console.WriteLine($"Number of users in team of project(description > 20 and count of tasks < 3): {item.NumberOfUsersInTeam}\n");
                            }
                            break;
                        }
                    case 0:
                        {
                            return;
                        }
                }
                Console.WriteLine("Press any key to return in menu");
                Console.ReadKey();
                Console.Clear();
            }
        }
    }
}
