﻿using bsa_collections.Models;
using System.Collections.Generic;

namespace bsa_collections.DTO
{
    public class ProjectMainInfoDTO
    {
        public Project Project { get; set; }
        public Task LongestTaskByDescription { get; set;}
        public Task ShortetTaskByName { get; set; }
        public int NumberOfUsersInTeam { get; set; }
    }
}
