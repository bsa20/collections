﻿using bsa_collections.Models;
using System.Collections.Generic;

namespace bsa_collections.DTO
{
    public class TeamDTO
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public IEnumerable<User> Users { get; set; }
    }
}
