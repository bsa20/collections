﻿using bsa_collections.Models;
using System.Collections.Generic;

namespace bsa_collections.DTO
{
    public class UserWithTasksDTO
    {
        public User User { get; set; }
        public IEnumerable<Task> Tasks { get; set; }
    }
}
