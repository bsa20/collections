﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace bsa_collections.DTO
{
    public class TaskDTO
    {
        public int Id { get; set; }
        public string Name { get; set; }
    }
}
