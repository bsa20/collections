﻿using bsa_collections.Models;

namespace bsa_collections.DTO
{
    public class UserMainInfoDTO
    {
        public User User { get; set; }
        public Project LastProject { get; set; }
        public int NumberOfTasksOfLastProject { get; set; }
        public int NumberOfNotFinishedTasks { get; set; }
        public Task LongestTask { get; set; }
    }
}
