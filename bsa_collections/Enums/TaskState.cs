﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace bsa_collections.Enums
{
    public enum TaskState
    {
        Created,
        Started,
        Finished,
        Canceled
    }
}
