﻿using bsa_collections.Enums;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace bsa_collections.Models
{
    public class Task
    {
        public int Id { get; set; }
        public string? Name { get; set; }
        public string? Description { get; set; }
        public DateTime CreatedAt { get; set; }
        public DateTime FinishedAt { get; set; }
        public TaskState State { get; set; }

        public int ProjectId { get; set; }
        public Project Project { get; set; }

        public int PerformerId { get; set; }
        public User Performer { get; set; }
    }
}
